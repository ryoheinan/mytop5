# mytop5

自分の〇〇top5（自分の好きな曲TOP5など）を、SNSにシェアできるWebアプリ



## 開発方針

なるべくGitHubフローに従ってください（issueごとに別ブランチを立ててマージリクエストする感じ）  

[参考MOOCsページ](https://moocs.iniad.org/courses/2020/CS104/02/02)  



### ブランチ名について

ブランチ名にissue番号を含めるとGitLab上でissueと紐づくのでそうしてください。  
例えばissue番号12だったら`12-hoge-fuga`みたいな

[参考ページ](https://qiita.com/luccafort/items/c91e817e78f1167221cc)



## 開発環境構築方法

ライブラリのバージョン管理とかをやりやすくするために[Poetry](https://python-poetry.org/)を使います。  

Python 3.7.3 を用います。  

macOS か WSLを想定しています。

### 1. Poetryのインストール

ターミナルで  

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

を実行しインストール。完了したら一応ターミナルを再起動する。

次にターミナルで  

```bash
poetry config virtualenvs.in-project true
```

を実行してください。  

(これで仮想環境がプロジェクト内に作成されるようになる)  



### 2. 仮想環境のセットアップ  

プロジェクトのフォルダに移動して、`poetry install`を実行。  

これでプロジェクトに必要なライブラリが自動でインストールされます。

`poetry shell`でシェルの中に入って`python -V`を実行し、3.7.3（2系でなければ）であればOK



### 3. ローカル設定ファイルのダウンロード

Herokuなどのサーバーにデプロイした時を見据えて、ローカル開発用のlocal_settings.pyを設定します。  

[ここ](https://drive.google.com/file/d/1ilzaSeFveSYW8tVj_52hEvs8DIoZq2rH/view?usp=sharing)からダウンロードして、configフォルダ内にファイルを置いてください。



### 4. VSCodeの設定（Optional）

mytop5フォルダ内に作成される`.vscode`フォルダ内の`settings.json`に  

（jsonファイルはmytop5をVSCodeで開いて、左下のPythonのバージョン選択をしたときに作られるはず）

```json
{
	"python.linting.pylintEnabled": false,
	"python.linting.flake8Enabled": true,
	"editor.formatOnSave": true,
    "python.linting.flake8Args": ["--ignore=E501"],
}
```

を設定しておくと自動整形等が有効になって楽  



## Poetryに関するメモ

- ライブラリを追加する時は、`pip install hoge`じゃなくて`poetry add hoge`
- なんかエラー起きたら、`poetry update`で最新の状態に更新する
- `runserver`などを実行する際は、`poetry shell`で仮想環境のシェルの中に入ってから実行する

