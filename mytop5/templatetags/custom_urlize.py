from django import template
register = template.Library()


@register.filter(name="custom_urlize", is_safe=True)
def custom_urlize(text):
    return text.replace('<a ', '<a target="_blank" rel="noopener noreferrer nofollow" ')
