import io
import os
import re
import math
import base64
from PIL import Image, ImageDraw, ImageFont


def fntSetting(fntType, fntSize):
    fntSrc = ""
    if fntType == "b":
        fntSrc = os.path.join(os.path.dirname(
            __file__), 'NotoSansJP-Bold.otf')
    elif fntType == "m":
        fntSrc = os.path.join(os.path.dirname(__file__),
                              'NotoSansJP-Medium.otf')
    elif fntType == "r":
        fntSrc = os.path.join(os.path.dirname(__file__),
                              'NotoSansJP-Regular.otf')
    else:
        raise Exception(f"{fntType}は無効なfntTypeです")
    fnt = ImageFont.truetype(
        fntSrc, fntSize)
    return fnt


def fntSizeSetting(word, baseSize):
    wordLen = len(word)
    fntSize = baseSize
    if re.search(r"[^ -~｡-ﾟ\t]{4,}", word):
        # 2バイト文字が4つ以上含まれているとき
        fntSize = baseSize - 8
        if 9 <= wordLen and baseSize == 53:
            fntSize = fntSize - math.ceil(wordLen * 1.4)
        elif 7 <= wordLen and baseSize == 42:
            fntSize = fntSize - math.ceil(wordLen * 1.4)
        elif 16 <= wordLen and baseSize == 50:
            fntSize = 45 - math.ceil(wordLen * 0.82)
        elif 13 <= wordLen and baseSize == 50:
            fntSize = 45 - math.ceil(wordLen * 0.55)
    elif baseSize == 50:
        pass
    else:
        if 13 <= wordLen:
            fntSize = fntSize - math.ceil(wordLen * 1.2)
    return fntSize


def rankWrite(draw, word, rank):
    x = 0
    y = 0
    fntSize = 10
    fntType = "r"
    if rank == 1:
        fntType = "b"
        x = 399
        y = 270
        fntSize = fntSizeSetting(word, 53)
    elif rank == 2:
        fntType = "m"
        x = 399
        y = 495
        fntSize = fntSizeSetting(word, 53)
    elif rank == 3:
        x = 942
        y = 164
        fntSize = fntSizeSetting(word, 53)
    elif rank == 4:
        x = 942
        y = 335
        fntSize = fntSizeSetting(word, 53)
    elif rank == 5:
        x = 942
        y = 510
        fntSize = fntSizeSetting(word, 53)

    draw.text((x, y), word, font=fntSetting(
        fntType, fntSize), anchor="mm", fill="black")
    return None


def imgGenerator(data, title, displayName, imgType):
    width = 1200
    height = 630
    if imgType == "instagram":
        width = 630
        height = 630
    """
    elif imgType == "line":
        width = 1200
        height = 630
    """

    img = Image.new("RGB", (width, height), (255, 255, 255))  # image生成

    with open("mytop5/img_base.txt", encoding="utf-8") as f:
        imgBaseData = f.read()
    imgBaseBytes = imgBaseData.encode("utf-8")
    imgBaseDecoded = base64.b64decode(imgBaseBytes)
    imgBase = Image.open(io.BytesIO(imgBaseDecoded))
    img.paste(imgBase, (0, 0))  # 大枠をimgに貼り付け

    draw = ImageDraw.Draw(img)  # imgの編集インスタンス生成

    for item in data:
        rankWrite(draw, item.title, item.rank)

    # ユーザーの名前とタイトル
    draw.text((67, 50), f"@{displayName}の", font=fntSetting(
        "b", 28), fill="black")
    titleSize = fntSizeSetting(title, 50)
    draw.text((67, 84), title, font=fntSetting(
        "b", titleSize), fill="black")

    buf = io.BytesIO()
    img.save(buf, format="PNG")

    base64Img = base64.b64encode(buf.getvalue()).decode().replace("'", "")
    return base64Img
