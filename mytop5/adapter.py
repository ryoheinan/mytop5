from allauth.socialaccount.adapter import DefaultSocialAccountAdapter


class CustomAdapter(DefaultSocialAccountAdapter):
    def save_user(self, request, sociallogin, form=None):
        if sociallogin.account.provider == 'twitter':
            # Twitterの表示名をニックネームとして登録する処理
            user = super(CustomAdapter, self).save_user(
                request, sociallogin, form)
            twitter_name = sociallogin.account.extra_data['screen_name']
            user.sns_type = "T"
            if twitter_name != "":
                user.display_name = twitter_name
            else:
                user.display_name = "ニックネーム"
            user.save()

        elif sociallogin.account.provider == 'line':
            # LINEの表示名をニックネームとして登録する処理
            user = super(CustomAdapter, self).save_user(
                request, sociallogin, form)
            line_display_name = sociallogin.account.extra_data['displayName']
            if line_display_name != "":
                user.display_name = line_display_name
            else:
                user.display_name = "ニックネーム"
            user.save()
