import re
from django import forms


class RankingForm(forms.Form):
    title = forms.CharField(max_length=18, label="ランキング名")
    description = forms.CharField(
        max_length=300, widget=forms.Textarea, label="ランキングの説明", required=False)


class DetailForm(forms.Form):
    dtitle1 = forms.CharField(max_length=20, label="名前")
    ddescription1 = forms.CharField(label="ランキングの説明", required=False)
    dtitle2 = forms.CharField(max_length=20, label="名前")
    ddescription2 = forms.CharField(label="ランキングの説明", required=False)
    dtitle3 = forms.CharField(max_length=20, label="名前")
    ddescription3 = forms.CharField(label="ランキングの説明", required=False)
    dtitle4 = forms.CharField(max_length=20, label="名前")
    ddescription4 = forms.CharField(label="ランキングの説明", required=False)
    dtitle5 = forms.CharField(max_length=20, label="名前")
    ddescription5 = forms.CharField(label="ランキングの説明", required=False)


class SettingsForm(forms.Form):
    NAME_REGEX = re.compile(r"^[a-zA-Z0-9._]{1,30}$")
    SNS_TYPE = (
        ("T", "Twitter"),
        ("I", "Instagram"),
        ("N", "なし"),
    )
    email = forms.EmailField(
        label="メールアドレス",
        max_length=255,
        widget=forms.TextInput(attrs={"placeholder": "mytop5@example.com"})
    )
    display_name = forms.RegexField(
        regex=NAME_REGEX, label="リンクするSNSプロフィールのユーザー名（@を除く）", widget=forms.TextInput(attrs={"placeholder": "example_name"}))
    sns_type = forms.ChoiceField(
        choices=SNS_TYPE, widget=forms.Select, label="リンクするSNSの種類")
