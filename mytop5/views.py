import base64
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from . import image
from .models import Ranking, Detail
from .forms import RankingForm, DetailForm, SettingsForm


def top(request):
    # top画面
    return render(request, "mytop5/top.html")


@login_required
def home(request):
    # マイページ
    userData = get_object_or_404(get_user_model(), id=request.user.id)
    rankingInfo = Ranking.objects.filter(
        user=userData).order_by("-created_date")
    return render(request, "mytop5/home.html", {"rankingInfo": rankingInfo, "userData": userData})


@login_required
def create(request):
    userData = get_object_or_404(get_user_model(), id=request.user.id)
    if request.method == "POST":
        rform = RankingForm(request.POST)
        dform = DetailForm(request.POST)
        if rform.is_valid() and dform.is_valid():
            rankingData = Ranking.objects.create(
                title=rform.cleaned_data["title"],
                description=rform.cleaned_data["description"],
                user=userData,
            )

            rankObj = []
            for i in range(1, 6):
                rankObj.append(Detail(
                    title=dform.cleaned_data[f"dtitle{i}"],
                    description=dform.cleaned_data[f"ddescription{i}"],
                    rank=i,
                    ranking=rankingData)
                )
            Detail.objects.bulk_create(rankObj)
            rankObj.clear()

            return redirect("detail", rankingData.id)

    return render(request, "mytop5/create.html", {"userData": userData})


@login_required
def edit(request, pk):
    userData = get_object_or_404(get_user_model(), id=request.user.id)
    rankingData = get_object_or_404(Ranking, user=userData, id=pk)
    if pk != rankingData.id:
        # 本当にランキング情報が存在するかどうか
        raise Http404
    detailData = Detail.objects.filter(ranking=rankingData)
    if request.method == "POST":
        if "ranking_delete" in request.POST:
            rankingData.delete()
            return redirect("home")

        rform = RankingForm(request.POST)
        dform = DetailForm(request.POST)
        if rform.is_valid() and dform.is_valid():
            rankingData.title = rform.cleaned_data["title"]
            rankingData.description = rform.cleaned_data["description"]
            rankingData.user = userData
            rankingData.save()

            i = 1
            for item in detailData:
                item.title = dform.cleaned_data[f"dtitle{ i }"]
                item.description = dform.cleaned_data[f"ddescription{ i }"]
                item.ranking = rankingData
                item.save()
                i += 1
        return redirect("detail", rankingData.id)

    return render(request, "mytop5/edit.html", {"userData": userData, "rankingData": rankingData, "detailData": detailData})


def detail(request, pk):
    # ランキング詳細画面
    isReal = False
    rankingInfo = get_object_or_404(Ranking, id=pk)
    rankData = Detail.objects.filter(ranking=rankingInfo).order_by("rank")
    if rankingInfo.user == request.user:
        isReal = True
    return render(request, "mytop5/detail.html", {"rankingInfo": rankingInfo, "rankData": rankData, "isReal": isReal})


@login_required
def settingsView(request, pk):
    if pk != request.user.id:
        # 本当に本人が本人の情報を確認しようとしているか
        raise Http404
    userData = get_object_or_404(get_user_model(), id=request.user.id)
    if request.method == "POST":
        form = SettingsForm(request.POST)
        if form.is_valid():
            userData.email = form.cleaned_data["email"]
            userData.display_name = form.cleaned_data["display_name"]
            userData.sns_type = form.cleaned_data["sns_type"]
            userData.save()
        return redirect("settings", userData.id)

    form = SettingsForm(
        initial={
            "email": userData.email,
            "display_name": userData.display_name,
            "sns_type": userData.sns_type,
        }
    )
    return render(request, "mytop5/settings.html", {"userData": userData, "form": form})


def imgView(request, imgType, pk):
    ranksetData = get_object_or_404(Ranking, id=pk)
    displayName = ranksetData.user.display_name
    title = ranksetData.title
    rankData = Detail.objects.filter(ranking=pk).order_by("rank")
    if not rankData:
        raise Http404
    rankDataset = []
    for item in rankData:
        rankDataset.append(item)
    imgBase64 = image.imgGenerator(
        rankDataset, title, displayName, imgType)  # base64エンコードされた文字列を受け取り
    binary = base64.b64decode(imgBase64)
    return HttpResponse(binary, content_type="image/png")
