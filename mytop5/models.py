from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core import validators
from django.conf import settings
import datetime


class MyUserManager(BaseUserManager):
    def create_user(self, email, display_name, sns_type, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            display_name=display_name,
            sns_type=sns_type,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, display_name, sns_type, password=None):
        """
        Creates and saves a User with the given email and password.
        """

        user = self.create_user(
            email,
            password=password,
            display_name=display_name,
            sns_type=sns_type
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    class Meta:
        verbose_name_plural = 'ユーザー情報'
    SNS_TYPE = [
        ("T", "https://twitter.com/"),
        ("I", "https://www.instagram.com/"),
        ("N", "なし"),
    ]
    email = models.EmailField(
        verbose_name='メールアドレス',
        max_length=255,
        unique=True,
    )
    display_name = models.CharField(max_length=30, verbose_name="SNSのユーザー名")
    sns_type = models.CharField(
        max_length=4, choices=SNS_TYPE, verbose_name="SNSの種類", default="N")
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['display_name', 'sns_type']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin


class Ranking(models.Model):
    class Meta:
        verbose_name_plural = "ランキングの集合"
    title = models.CharField(max_length=18, verbose_name="ランキング名")
    description = models.TextField(verbose_name="ランキングの説明", blank=True)
    created_date = models.DateTimeField(
        default=datetime.datetime.now, verbose_name="作成日時")
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)  # ユーザーの外部参照キー


class Detail(models.Model):
    class Meta:
        verbose_name_plural = "ランキング詳細情報"
    title = models.CharField(max_length=20, verbose_name="名前")
    rank = models.IntegerField(validators=[validators.MinValueValidator(1),
                                           validators.MaxValueValidator(5)], verbose_name="ランク")
    description = models.TextField(verbose_name="ランキングの説明", blank=True)
    ranking = models.ForeignKey(Ranking,
                                on_delete=models.CASCADE)  # ランキングの外部参照キー
