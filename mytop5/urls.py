from django.urls import path
from . import views

urlpatterns = [
    path("", views.top, name="top"),
    path("home/", views.home, name="home"),
    path("create/", views.create, name="create"),
    path('detail/<int:pk>/', views.detail, name="detail"),
    path('edit/<int:pk>/', views.edit, name="edit"),
    path('settings/<int:pk>/', views.settingsView, name="settings"),
    path("img/<str:imgType>/<int:pk>/", views.imgView),
]
